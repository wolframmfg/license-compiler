# License Compiler

Generates a single HTML file containing the licenses text for all depdencies of a project.

# Arguments

- `search` sets the start directory for recursive package searching. Optional, defaults to the current directory.
- `title` sets the title of the resulting HTML document. Optional, defaults to "Open-source licenses".
- `out` sets the out file path. Optional, defaults to the standard output stream.

# Examples

```shell
python license_compiler.py --search ../lib
```

```shell
python license_compiler.py --title "3rd-Party Software Licenses" --out 3rdPartyLicenses.html
```

```shell
python license_compiler.py | gz > licenses.html.gz
```

Python (with Poetry) project on Windows:

```shell
python license_compiler.py --search "%LocalAppData%\pypoetry\Cache\virtualenvs\...\Lib\site-packages"
```

Node.js project:

```shell
python license_compiler.py --search ./node_modules
```

Go project on Windows:

```shell
python license_compiler.py --search "%UserProfile%\go\pkg\mod"
```
