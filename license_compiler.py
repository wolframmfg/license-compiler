from argparse import ArgumentParser
from glob import iglob
from html import escape
from os import fspath
from os.path import expandvars, join
from pathlib import Path
import re
from sys import stdout, stderr

python_package_regex = re.compile(r"(.*?)\W(\d+\.\d+(\.\d+)?)\.dist-info")

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--search", default=".")
    parser.add_argument("--out", default="-")
    parser.add_argument("--title", default="Open-source licenses")
    args = parser.parse_args()

    licenses = {}

    for file_path in iglob(fspath(join(expandvars(args.search), "**", "LICENSE*")), recursive=True):
        file_path = Path(file_path)

        if file_path.is_dir():
            continue

        if file_path.suffix in {".py", ".pyc"}:
            print(f"Skipping file '{file_path}'", file=stderr)
            continue

        package_name = file_path.parent.name

        if package_name.startswith("~"):
            print(f"Skipping package '{package_name}'", file=stderr)
            continue

        if match := python_package_regex.match(package_name):
            package_name = match[1]

        if "@" in package_name:
            package_name = package_name.split("@")[0]

        try:
            with open(file_path, encoding="utf-8") as license_file:
                license_text = license_file.read()

                if "WTFPL" in license_text:
                    print(f"License for '{package_name}' may contain unprofessional language.", file=stderr)

                license_html = escape(license_text)
        except Exception as e:
            print(f"Error reading '{file_path}': {e}", file=stderr)
        else:
            licenses[package_name] = license_html

    if args.out == "-":
        output_file = stdout
    else:
        output_file = open(args.out, "w", encoding="utf-8")

    output_file.write(
        f"""<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>{args.title}</title>
</head>
<body>
<h1>{args.title}</h1>
""")

    for license_name in sorted(licenses.keys(), key=lambda k: k.lower()):
        output_file.write(f"<h2>{license_name}</h2>\n")
        output_file.write(f"<pre>{licenses[license_name]}</pre>\n")

    output_file.write(
        """
</body>
</html>""")

    if output_file != stdout:
        output_file.close()
